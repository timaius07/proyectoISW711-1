﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class ReunionController : Controller
    {
		// GET: Reunion
		public ActionResult Index()
		{						//es el context
			using (var db = new ReunioneEntities())
			{
				return View(db.Reunion.ToList());
			}

		}

		public ActionResult Agregar()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Agregar(Reunion re)
		{
			//verifica nuevamente si algun dato del modelo no es valido
			if (!ModelState.IsValid)
				return View();
			try
			{						
				using (var db = new ReunioneEntities())
				{
					db.Reunion.Add(re);
					db.SaveChanges();
					//return RedirectToAction("Index");
					return View();
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("Error al Agregar Reunion", ex);
				return View();
			}
		}

		//retorna los datos de clientes de la vista cliente
		//con la ayuda de la vista parcial
		public ActionResult ListarUsuario()
		{
			using (var db = new ReunioneEntities())
			{
				return PartialView(db.Usuarios.ToList());
			}
		}
		public static string NombreUsuario(int IdCliente)
		{
			using (var db = new ReunioneEntities())
			{
				return db.Usuarios.Find(IdCliente).Nombre_Usuario;
			}
		}

		public ActionResult Detalles(int id)
		{
			using (var db = new ReunioneEntities())
			{
				Reunion cl = db.Reunion.Find(id);
				return View(cl);
			}
		}

		public ActionResult Editar(int id)
		{
			try
			{
				using (var db = new ReunioneEntities())
				{
					//si es nulo dice que no existe, sino selecciona el primero
					//Cliente cont = db.Cliente.Where(c => c.id == id).FirstOrDefault();
					Reunion re = db.Reunion.Find(id);
					return View(re);
				}
			}
			catch (Exception)
			{

				throw;
			}
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Editar(Reunion c)
		{
			//c datos que vienen del formulario
			try
			{
				if (!ModelState.IsValid)
					return View();
				using (var db = new ReunioneEntities())
				{
					//cl es la reunion que se encontro en la BD
					Reunion cl = db.Reunion.Find(c.Id);
					cl.Titulo_Reunion = c.Titulo_Reunion;
					cl.Fecha_Hora = c.Fecha_Hora;
					cl.Id_Usuario = c.Id_Usuario;
					cl.Virtual = c.Virtual;

					db.SaveChanges();
				}

				return RedirectToAction("Index");
			}
			catch (Exception)
			{

				throw;
			}

		}
		public ActionResult Eliminar(int id)
		{
			using (var db = new ReunioneEntities())
			{
				Reunion co = db.Reunion.Find(id);
				db.Reunion.Remove(co);
				db.SaveChanges();
				return RedirectToAction("Index");
			}


		}
		public ActionResult Buscar(string palabra)
		{
			IEnumerable<Reunion> reunion;

			using (var bd = new ReunioneEntities())
			{
				reunion = bd.Reunion;

				if (!String.IsNullOrEmpty(palabra))
				{
					reunion = reunion.Where(l => l.Titulo_Reunion.Contains(palabra));
				}

				reunion = reunion.ToList();
			}

			return View(reunion);
		}
	}
}