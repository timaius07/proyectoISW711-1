﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexAdm()
        {
            try
            {
                using (var db = new LoginContext())
                {
                    return View(db.Usuarios.ToList());
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

		[HttpPost]
		public ActionResult Autorizar(Usuario usuario)
		{
			using (var db = new LoginContext())
			{
				var userDetails= db.Usuarios.Where(x => x.Nombre_Usuario == usuario.Nombre_Usuario && x.Contrasena == usuario.Contrasena).FirstOrDefault();
				if(userDetails == null)
				{
					usuario.Error = "Error Usuario o Contraseña Invalidos";
					return View("Index", usuario);
				}
				else
				{
					Session["userID"] = userDetails.Id;
					Session["userName"] = userDetails.Nombre_Usuario;
					Session["userRol"] = userDetails.Rol_Usuario;
					if (Session["userRol"].ToString() == "Admin")
					{
						return RedirectToAction("Index", "Home");
					}
					else
					{
						return RedirectToAction("About", "Home");
					}
					
				}
			}	
		}
		public ActionResult LogOut()
		{
			int usuarioId = (int)Session["userID"];
			Session.Abandon();
			return RedirectToAction("Index", "Login");
		}
        public ActionResult Inicio()
        {
            try
            {
                using (var db = new LoginContext())
                {
                    return View(db.Usuarios.ToList());
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ActionResult Agregar()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Agregar(Usuario c)
        {
            //verifica nuevamente si algun dato del modelo no es valido
            if (!ModelState.IsValid)
                return View();
            try
            {
                using (var db = new LoginContext())
                {
                    db.Usuarios.Add(c);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error al Registrarse", ex);
                return View();
            }
        }

		public ActionResult AgregarAdm()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AgregarAdm(Usuario c)
		{
			//verifica nuevamente si algun dato del modelo no es valido
			if (!ModelState.IsValid)
				return View();
			try
			{
				using (var db = new LoginContext())
				{
					db.Usuarios.Add(c);
					db.SaveChanges();
					return RedirectToAction("Inicio", "Login");
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("Error al Registrarse", ex);
				return View();
			}
		}

		public ActionResult Editar(int id)
        {
            try
            {
                using (var db = new LoginContext())
                {
                    //si es nulo dice que no existe, sino selecciona el primero
                    //Cliente cl = db.Cliente.Where(c => c.id == id).FirstOrDefault();
                    Usuario cl = db.Usuarios.Find(id);
                    return View(cl);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(Usuario c)
        {
            //c datos que vienen del formulario
            try
            {
                if (!ModelState.IsValid)
                    return View();
                using (var db = new LoginContext())
                {
                    //cl es el cliente que se encontro en la BD
                    Usuario cl = db.Usuarios.Find(c.Id);
                    cl.Nombre_Usuario = c.Nombre_Usuario;
                    cl.Contrasena = c.Contrasena;
                    cl.Rol_Usuario = c.Rol_Usuario;
                    db.SaveChanges();
                }

                return RedirectToAction("Inicio", "Login");
            }
            catch (Exception)
            {

                throw;
            }

        }
        public ActionResult Detalles(int id)
        {
            using (var db = new LoginContext())
            {
                Usuario cl = db.Usuarios.Find(id);
                return View(cl);
            }
        }

        public ActionResult Eliminar(int id)
        {
            using (var db = new LoginContext())
            {
                Usuario cl = db.Usuarios.Find(id);
                db.Usuarios.Remove(cl);
                db.SaveChanges();
                return RedirectToAction("Inicio");
            }


        }


    }
}
