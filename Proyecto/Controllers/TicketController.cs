﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class TicketController : Controller
    {
        // GET: Ticket
        public ActionResult Index()
        {
			try
			{
				using (var db = new Ticket_Context())
				{
					return View(db.Tickets.ToList());
				}
			}
			catch (Exception)
			{

				throw;
			}
		}
		public ActionResult IndexAdmin()
		{
			try
			{
				using (var db = new Ticket_Context())
				{
					return View(db.Tickets.ToList());
				}
			}
			catch (Exception)
			{

				throw;
			}
		}
		public ActionResult Agregar()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Agregar(Tickets c)
		{
			//verifica nuevamente si algun dato del modelo no es valido
			if (!ModelState.IsValid)
				return View();
			try
			{
				using (var db = new Ticket_Context())
				{
					db.Tickets.Add(c);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("Error al Agregar Ticket", ex);
				return View();
			}
		}
		public ActionResult Editar(int id)
		{
			try
			{
				using (var db = new Ticket_Context())
				{
					//si es nulo dice que no existe, sino selecciona el primero
					//Cliente cl = db.Cliente.Where(c => c.id == id).FirstOrDefault();
					Tickets cl = db.Tickets.Find(id);
					return View(cl);
				}
			}
			catch (Exception)
			{

				throw;
			}
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Editar(Tickets c)
		{
			//c datos que vienen del formulario
			try
			{
				if (!ModelState.IsValid)
					return View();
				using (var db = new Ticket_Context())
				{
					//cl es el cliente que se encontro en la BD
					Tickets cl = db.Tickets.Find(c.Id);
					cl.Titulo_Problema = c.Titulo_Problema;
					cl.Detalle= c.Detalle;
					cl.Reporta = c.Reporta;
					cl.Cliente = c.Cliente;
					cl.EstadoActual = c.EstadoActual;
					db.SaveChanges();
				}

				return RedirectToAction("Index");
			}
			catch (Exception)
			{

				throw;
			}

		}
		public ActionResult ListarCliente()
		{
			using (var db = new Ticket_Context())
			{
				return PartialView(db.Cliente.ToList());
			}
		}
		public static string NombreCliente(int IdCliente)
		{
			using (var db = new Ticket_Context())
			{
				return db.Cliente.Find(IdCliente).Nombre;
			}
		}
		public ActionResult Detalles(int id)
		{
			using (var db = new Ticket_Context())
			{
				Tickets cl = db.Tickets.Find(id);
				return View(cl);
			}
		}
		public ActionResult Eliminar(int id)
		{
			using (var db = new Ticket_Context())
			{
				Tickets co = db.Tickets.Find(id);
				db.Tickets.Remove(co);
				db.SaveChanges();
				return RedirectToAction("Index");
			}
		}
	}
}