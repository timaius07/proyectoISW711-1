﻿
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Proyecto.Controllers
{
	
	public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {
			try
			{
				using (var db = new ProyectoContext())
				{
					return View(db.Cliente.ToList());
				}
			}
			catch (Exception)
			{

				throw;
			}
			
        }
		public ActionResult Agregar()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Agregar(Cliente c)
		{
			//verifica nuevamente si algun dato del modelo no es valido
			if (!ModelState.IsValid)
				return View();
			try
			{
				using (var db = new ProyectoContext())
				{
					db.Cliente.Add(c);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
			}catch (Exception ex)
			{
				ModelState.AddModelError("Error al Agregar Cliente", ex);
				return View();
			}
		}
		public ActionResult Editar(int id)
		{
			try
			{
				using (var db = new ProyectoContext())
				{
					//si es nulo dice que no existe, sino selecciona el primero
					//Cliente cl = db.Cliente.Where(c => c.id == id).FirstOrDefault();
					Cliente cl = db.Cliente.Find(id);
					return View(cl);
				}
			}
			catch (Exception)
			{

				throw;
			}		
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Editar(Cliente c)
		{
			//c datos que vienen del formulario
			try
			{
				if (!ModelState.IsValid)
					return View();
				using (var db = new ProyectoContext())
				{
					//cl es el cliente que se encontro en la BD
					Cliente cl = db.Cliente.Find(c.id);
					cl.Nombre = c.Nombre;
					cl.Ced_Juridica = c.Ced_Juridica;
					cl.Pag_Web = c.Pag_Web;
					cl.Telefono = c.Telefono;
					cl.Sector = c.Sector;

					db.SaveChanges();
				}

				return RedirectToAction("Index");
			}
			catch (Exception)
			{

				throw;
			}
			
		}
		public ActionResult Detalles(int id)
		{
			using (var db= new ProyectoContext())
			{
				Cliente cl = db.Cliente.Find(id);
				return View(cl);
			}
		}
		public ActionResult Eliminar( int id)
		{
			using (var db = new ProyectoContext())
			{
				Cliente cl = db.Cliente.Find(id);
				db.Cliente.Remove(cl);
				db.SaveChanges();
				return RedirectToAction("Index");
			}


		}


	}
}