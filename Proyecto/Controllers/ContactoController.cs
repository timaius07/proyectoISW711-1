﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class ContactoController : Controller
    {
		
		// GET: Contacto
		public ActionResult Index()
        {
			using (var db = new ContactoContext())
			{
				return View(db.Contacto.ToList());
			}
				
        }
		public ActionResult Agregar()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Agregar(Contacto c)
		{
			//verifica nuevamente si algun dato del modelo no es valido
			if (!ModelState.IsValid)
				return View();
			try
			{	
				using (var db = new ContactoContext())
				{
					db.Contacto.Add(c);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("Error al Agregar Contacto", ex);
				return View();
			}
		}
		public ActionResult Editar(int id)
		{
			try
			{
				using (var db = new ContactoContext())
				{
					//si es nulo dice que no existe, sino selecciona el primero
					//Cliente cont = db.Cliente.Where(c => c.id == id).FirstOrDefault();
					Contacto cont = db.Contacto.Find(id);
					return View(cont);
				}
			}
			catch (Exception)
			{

				throw;
			}


		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Editar(Contacto c)
		{
			//c datos que vienen del formulario
			try
			{
				if (!ModelState.IsValid)
					return View();
				using (var db = new ContactoContext())
				{
					//cont es el cliente que se encontro en la BD
					Contacto cl = db.Contacto.Find(c.Id);
					cl.Id_Cliente = c.Id_Cliente;
					cl.Nombre = c.Nombre;
					cl.Apellidos = c.Apellidos;
					cl.Correo_Electronico = c.Correo_Electronico;
					cl.Telefono = c.Telefono;

					db.SaveChanges();
				}

				return RedirectToAction("Index");
			}
			catch (Exception)
			{

				throw;
			}

		}
		//retorna los datos de clientes de la vista cliente
		//con la ayuda de la vista parcial
		public ActionResult ListaCliente()
		{
			using (var db = new ContactoContext())
			{
				return PartialView(db.Cliente.ToList());
			} 
		}
		public static string NombreCliente (int IdCliente)
		{
			using (var db = new ContactoContext())
			{
				return db.Cliente.Find(IdCliente).Nombre;
			}
		}
		public ActionResult Detalles(int id)
		{
			using (var db = new ContactoContext())
			{
				Contacto cl = db.Contacto.Find(id);
				return View(cl);
			}
		}
		public ActionResult Eliminar(int id)
		{
			using (var db = new ContactoContext())
			{
				Contacto co = db.Contacto.Find(id);
				db.Contacto.Remove(co);
				db.SaveChanges();
				return RedirectToAction("Index");
			}
		}
	}
}