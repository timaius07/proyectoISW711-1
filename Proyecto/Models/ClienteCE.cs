﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
	public class ClienteCE
	{
		
		[Required]
		public string Nombre { get; set; }
		[Required]
		public string Ced_Juridica { get; set; }
		[Required]
		public string Pag_Web { get; set; }
		[Required]
		public string Telefono { get; set; }
		[Required]
		public string Sector { get; set; }
	}
	[MetadataType (typeof(ClienteCE))]

	public partial class Cliente
	{

	}
}